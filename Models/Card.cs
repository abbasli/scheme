﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
    
namespace Scheme.Models
{
    public class Card
    {
        public int CardId { get; set; }
        public string Name { get; set; }
        public List<UserCard> UserCards { get; set; }

        public Card()
        {
            UserCards = new List<UserCard>();
        }
    }
}
