﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheme.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Salt { get; set; }
        public string PassHash { get; set; }
        public Role Role { get; set; }
        public List<UserCard> UserCards { get; set; }

        public User()
        {
            UserCards = new List<UserCard>();
        }
    }

}
