﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheme.Models
{
    public class Notification
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public User User { get; set; }
        public Card Card { get; set; }
        public BoardColumn From { get; set; }
        public BoardColumn To { get; set; } // string or BoardColumn?
    }
}

