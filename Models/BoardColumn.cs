﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheme.Models
{
    public class BoardColumn
    {
        public int Id { get; set; }
        public DateTime Duration { get; set; }
        public List<Card> Cards { get; set; }
        public BoardColumnType Type { get; set; }
    }
}

