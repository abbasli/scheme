﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheme.Models
{

    public class ProjectContext : DbContext
    {
        public DbSet<BoardColumn> BoardColumns { get; set; }
        public DbSet<BoardColumnType> BoardColumnTypes { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserCard>()
                .HasKey(t => new { t.User, t.Card });

            modelBuilder.Entity<UserCard>()
                .HasOne(sc => sc.User)
                .WithMany(s => s.UserCards)
                .HasForeignKey(sc => sc.UserId);

            modelBuilder.Entity<UserCard>()
                .HasOne(sc => sc.Card)
                .WithMany(c => c.UserCards)
                .HasForeignKey(sc => sc.CardId);
        }
    }
}
