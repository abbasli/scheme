﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheme.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BoardColumn> BoardColumns { get; set; }
        public BoardColumn Sprint { get; set; }
        public BoardColumn BackLog { get; set; }
    }
}
